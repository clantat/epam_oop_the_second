package com.clantat;

public interface MarksCalculationService {
    double meanStudentMarks(Student student);
    int excellentStudents (Group group);
    int badStudents (Group group);
}
