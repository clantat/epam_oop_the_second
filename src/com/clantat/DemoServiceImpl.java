package com.clantat;

public class DemoServiceImpl implements DemoService {
    @Override
    public void start() {

        int []g = {3,4,5,5,4};
        StudentProgress ggg = new StudentProgress(g);
        Student Andrew = new Student("Andrew",19,ggg);

        int []h = {5,5,5,5,5};
        StudentProgress hhh = new StudentProgress(h);
        Student Artur = new Student("Artur",19,hhh);

        int []b = {2,5,5,3,5};
        StudentProgress bbb = new StudentProgress(b);
        Student Artem = new Student("Artem",19,bbb);

        int []c = {3,3,2,4,5};
        StudentProgress ccc = new StudentProgress(c);
        Student Anton = new Student("Anton",19,ccc);

        int []n = {2,2,2,3,4};
        StudentProgress nnn = new StudentProgress(n);
        Student Alex = new Student("Alex",19,nnn);

        Student [] mathematic = {Andrew,Artem,Artur,Anton,Alex};
        Group math = new Group("Mathematic",mathematic);
        MarksCalculationServiceImpl calculate = new MarksCalculationServiceImpl();
        System.out.println("Cредний балл учебной группы "+math.meanMarks(calculate));
        System.out.println();
        for (int i = 0; i <math.getGroup().length; i++) {
            System.out.println("Средняя оценка "+math.getGroup()[i].getName()+" равна "+calculate.meanStudentMarks(math.getGroup()[i]));
        }
        System.out.println();
        System.out.println("Число отличников "+ calculate.excellentStudents(math));
        System.out.println();
        System.out.println("Количество студентов, имеющих неудовлетворительно "+ calculate.badStudents(math));

    }
}
