package com.clantat;

public class Student {
    private String name;
    private int age;
    private StudentProgress numbers;

    public Student(String name, int age,StudentProgress numbers) {
        this.name = name;
        this.age = age;
        this.numbers = numbers;
    }
    public StudentProgress getNumbers() {
        return numbers;
    }

    public void setNumbers(StudentProgress numbers) {
        this.numbers = numbers;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
