package com.clantat;

public class StudentProgress {
    private int [] marks;

    public StudentProgress(int[] marks) {
        this.marks = marks;
    }

    public int[] getMarks() {
        return marks;
    }

    public void setMarks(int[] marks) {
        this.marks = marks;
    }
}
