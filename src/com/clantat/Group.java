package com.clantat;

public class Group {
    private String GroupName;
    private Student [] group;

    public Group(String groupName, Student[] group) {
        GroupName = groupName;
        this.group = group;
    }


    public String getGroupName() {
        return GroupName;
    }

    public void setGroupName(String groupName) {
        GroupName = groupName;
    }


    public Student[] getGroup() {
        return group;
    }

    public void setGroup(Student[] group) {
        this.group = group;
    }

    public double meanMarks (MarksCalculationServiceImpl meanStudent){
        double meanGroupMarks=0;
        for (int i = 0; i <group.length ; i++) {
            meanGroupMarks+=meanStudent.meanStudentMarks(group[i]);
        }
        meanGroupMarks=meanGroupMarks/group.length;
        return meanGroupMarks;
    }
}
