package com.clantat;

public class MarksCalculationServiceImpl implements MarksCalculationService {

    @Override
    public double meanStudentMarks(Student student) {
        double meanMark=0;
        for (int i = 0; i <student.getNumbers().getMarks().length; i++) {
            meanMark += student.getNumbers().getMarks()[i];
        }
        meanMark= meanMark/student.getNumbers().getMarks().length;
        return meanMark;
    }

    @Override
    public int excellentStudents (Group group){
        int count=0;
        for (int i = 0; i <group.getGroup().length; i++) {
            if(meanStudentMarks(group.getGroup()[i])>=4.5)count++;
        }
        return count;
    }

    @Override
    public int badStudents (Group group){
        int count=0;
        for (int i = 0; i <group.getGroup().length; i++) {
            if(meanStudentMarks(group.getGroup()[i])>=2.5&&meanStudentMarks(group.getGroup()[i])<3.5)count++;
        }
        return count;
    }
}
